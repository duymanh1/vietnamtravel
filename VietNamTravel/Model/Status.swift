//
//  Status.swift
//  VietNamTravel
//
//  Created by admin on 12/2/21.
//

import Foundation

struct StatusUser {
    
    var postImage = ""
    var content = ""
    var Poster:People
    var like = 0
    var idPost: Int
    
//    init(postImage: String, content: String, namePoster: String, like: Int, idPost: Int) {
//        self.postImage = postImage
//        self.content = content
//        self.namePoster = namePoster
//        self.like = like
//        self.idPost = idPost
//    }
    
}

struct Comment {
    
    var user: People
    var content = ""
    var idPost: Int?
    
}

struct AreaPlace {
    
    var areaImage = ""
    var tittle = ""
    
    init(tourImage: String, tittle: String) {
        self.areaImage = tourImage
        self.tittle = tittle
    }
    
}

struct Post {
    var image = ""
    var content = ""
    var like = 0
    var comment = ""
    
    init(image: String, content: String, like: Int, comment: String) {
        self.image = image
        self.content = content
        self.like = like
        self.comment = comment
    }
    
}

struct People {
    
    var avatarImage = ""
    var name = ""
    
    init(avatar: String, name: String) {
        self.avatarImage = avatar
        self.name = name
    }
    
}

struct InfoUser {
    var icon = ""
    var value = ""
    init(icon: String, value: String) {
        self.icon = icon
        self.value = value
    }
}

struct Message {
    var user: People?
    var sender: People?
    var message = ""
    init(user: People, sender: People, message: String) {
        self.user = user
        self.sender = sender
        self.message = message
    }
    
}


struct User: Codable {
    var fullName = ""
    var avatarImage = ""
    var gender = ""
    var birthday = ""
    var address = ""
    var email = ""
    
}
