//
//  HistorySearchTableViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/6/21.
//

import UIKit

class HistorySearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var historyCollectionView: UICollectionView!
    
    var arrayHistory = ["Hà Nội", "Hồ Chí Minh", "Nam Định", "Đà Nẵng", "Quảng Ninh"]
    var namePlace = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        historyCollectionView.delegate = self
        historyCollectionView.dataSource = self
        historyCollectionView.register(UINib(nibName: "HistorySearchCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HistorySearchCollectionViewCell")
        if let flowLayout = historyCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
              flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
           }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
}

extension HistorySearchTableViewCell: UICollectionViewDelegate {
    
}

extension HistorySearchTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayHistory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = historyCollectionView.dequeueReusableCell(withReuseIdentifier: "HistorySearchCollectionViewCell", for: indexPath) as? HistorySearchCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.historyLabel.text = arrayHistory[indexPath.row]
       return cell
    }
    
    
}
