//
//  HistorySearchCollectionViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/6/21.
//

import UIKit

class HistorySearchCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var historyLabel: UILabel!
    @IBOutlet weak var historyView: UIView!
    @IBOutlet weak var historyViewHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var viewLabel: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        historyView.layer.cornerRadius = 5
        viewLabel.layer.cornerRadius = 5
    }

}
