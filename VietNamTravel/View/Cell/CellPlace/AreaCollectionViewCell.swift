//
//  AreaCollectionViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/6/21.
//

import UIKit

class AreaCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var areaImage: UIImageView!
    @IBOutlet weak var areaNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
