//
//  PlaceTableViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/6/21.
//

import UIKit

protocol ChooseAreaDelegate
{
//    func passClickSeeAll()
    func selectArea(_ namePlace: String)
}

class PlaceTableViewCell: UITableViewCell {

    @IBOutlet weak var areaCollectionView: UICollectionView!
    
    var arrayArea = [AreaPlace(tourImage: "haNoi", tittle: "Ha Noi"), AreaPlace(tourImage: "tpHoChiMinh", tittle: "TP. Ho Chi Minh"),
                     AreaPlace(tourImage: "haNoi", tittle: "Ha Noi"), AreaPlace(tourImage: "tpHoChiMinh", tittle: "TP. Ho Chi Minh"),
                     AreaPlace(tourImage: "haNoi", tittle: "Ha Noi"), AreaPlace(tourImage: "tpHoChiMinh", tittle: "TP. Ho Chi Minh"),
                     AreaPlace(tourImage: "haNoi", tittle: "Ha Noi"), AreaPlace(tourImage: "tpHoChiMinh", tittle: "TP. Ho Chi Minh")]
    var delegate: ChooseAreaDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        areaCollectionView.delegate = self
        areaCollectionView.dataSource = self
        areaCollectionView.register(UINib(nibName: "AreaCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AreaCollectionViewCell")
//        if let flowLayout = areaCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
////            flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
//            let w = areaCollectionView.frame.width/3 - 20
//            flowLayout.estimatedItemSize = CGSize(width: w, height:w)
//        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

extension PlaceTableViewCell: UICollectionViewDelegate {
}

extension PlaceTableViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayArea.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = areaCollectionView.dequeueReusableCell(withReuseIdentifier: "AreaCollectionViewCell", for: indexPath) as? AreaCollectionViewCell else {
            return UICollectionViewCell()
        }
        let data = arrayArea[indexPath.row]
        cell.areaImage.image = UIImage(named: data.areaImage)
        cell.areaNameLabel.text = data.tittle
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.selectArea(arrayArea[indexPath.row].tittle)
    }
}
