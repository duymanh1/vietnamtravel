//
//  FollowerTableViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/15/21.
//

import UIKit

protocol FollowDelegate {
    func cellDelegate(_ people: User?)
}

class FollowerTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var arrayFriend = [User]()
    var delegate: FollowDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupUI() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "FollowerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FollowerCollectionViewCell")
    }
    
}

extension FollowerTableViewCell: UICollectionViewDelegate {
    
}

extension FollowerTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayFriend.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FollowerCollectionViewCell", for: indexPath) as? FollowerCollectionViewCell else {
            return UICollectionViewCell()
        }
        let data = arrayFriend[indexPath.row]
        cell.avatarImage.image = UIImage(named: data.avatarImage)
        cell.nameLabel.text = data.fullName
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.cellDelegate(arrayFriend[indexPath.row])
    }
    
}
