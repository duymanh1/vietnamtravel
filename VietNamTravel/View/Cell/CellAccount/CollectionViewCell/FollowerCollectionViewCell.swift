//
//  FollowerCollectionViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/15/21.
//

import UIKit

class FollowerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }

    func setupUI() {
        avatarImage.layer.cornerRadius = avatarImage.frame.height/2
    }
}
