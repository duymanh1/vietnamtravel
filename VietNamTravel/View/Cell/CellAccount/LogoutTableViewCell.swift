//
//  LogoutTableViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/15/21.
//

import UIKit

class LogoutTableViewCell: UITableViewCell {

    @IBOutlet weak var logoutView: UIView! {
        didSet {
            logoutView.layer.cornerRadius = logoutView.frame.height/2
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
