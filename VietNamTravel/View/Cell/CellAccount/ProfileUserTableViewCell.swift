//
//  ProfileUserTableViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/16/21.
//

import UIKit
protocol ProfileUserTableViewCellDelegate {
    func clickChatButton()
}

class ProfileUserTableViewCell: UITableViewCell {
 
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var bgView: UIView! {
        didSet {
            bgView.layer.cornerRadius = 8
            bgView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
    }
    @IBOutlet weak var avatarImage: UIImageView! {
        didSet {
            avatarImage.layer.cornerRadius = avatarImage.frame.height/2
        }
    }
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nickNameLabel: UILabel!
    @IBOutlet weak var messageButton: UIButton!{
        didSet {
            messageButton.layer.cornerRadius = messageButton.frame.height/2
            
        }
    }
    @IBOutlet weak var followButton: UIButton! {
        didSet {
            followButton.layer.cornerRadius = followButton.frame.height/2
        }
    }
    
    var delegate: ProfileUserTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func clickChatButton(_ sender: Any) {
        delegate?.clickChatButton()
    }
}
