//
//  ProfileTableViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/14/21.
//

import UIKit

protocol ProfileUserDelegate {
    func clickInfoUser()
}


class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImage: UIImageView!{
        didSet {
            avatarImage.layer.cornerRadius = avatarImage.frame.height/2
        }
    }
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var nickNameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    var nameAvatarImage = "bgLogin"
    var fullname = "Nguyễn Hùng Mạnh"
    var nickname = "MD"
    var phoneNumber = "0968140814"
    var email = "Manhdiep@gmail.com"
    
    var delegate: ProfileUserDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        setupUI()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        avatarImage.isUserInteractionEnabled = true
        avatarImage.addGestureRecognizer(tapGestureRecognizer)
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        fullnameLabel.isUserInteractionEnabled = true
        fullnameLabel.addGestureRecognizer(tapGestureRecognizer1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupUI() {
        
//        fullnameLabel.text = fullname
        nickNameLabel.text = nickname
        avatarImage.image = UIImage(named: nameAvatarImage)
        phoneNumberLabel.text = phoneNumber
        emailLabel.text = email
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        delegate?.clickInfoUser()
    }
    
    @IBAction func clickEditButton(_ sender: Any) {
    }
}
