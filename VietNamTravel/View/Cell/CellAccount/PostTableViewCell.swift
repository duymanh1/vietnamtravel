//
//  PostTableViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/16/21.
//

import UIKit

protocol PostCellDelegate {
    func isClickButtonCommnent(_ isClick: Bool, _ statusHeight: CGFloat, _ commentHeeight: CGFloat)
//    func setupPostHeight(_ statusHeight: CGFloat, _ commentHeeight: CGFloat)
}

class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImage: UIImageView! {
        didSet {
            avatarImage.layer.cornerRadius = avatarImage.frame.height/2
        }
    }
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var sections: [statusTable] = []
    var isClickComment = false
    var arrayComment1 = [String]()
    var arrayComment = [String]()
    var height: CGFloat = 0
    var heightStatus: CGFloat = 0
    var heightcomment: CGFloat = 0
    var delegate: PostCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        sections.removeAll()
        sections.append(contentsOf: [.status, .comment])
        arrayComment1 = ["Good", "Kẻ lữ khách là những con người mộng mơ dám biến những chuyến hành trình trong trí tưởng tượng thành sự thật", "Very Very Very Good!!!!!. Kẻ lữ khách là những con người mộng mơ dám biến những chuyến hành trình trong trí tưởng tượng thành sự thật", "Wonderful"]
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "StatusTableViewCell", bundle: nil), forCellReuseIdentifier: "StatusTableViewCell")
        tableView.register(UINib(nibName: "ConmentTableViewCell", bundle: nil), forCellReuseIdentifier: "ConmentTableViewCell")
        tableView.separatorStyle = .none
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
}

extension PostTableViewCell: UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
        let section = sections[indexPath.section]
        switch section {
        case .status:
            return UITableView.automaticDimension
        case .comment:
            return UITableView.automaticDimension

        }
    }
    
}

extension PostTableViewCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = sections[section]
        switch section {
        case .status:
            return 1
        case .comment:
            return arrayComment.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sections[indexPath.section]
        switch section {
        case .status:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "StatusTableViewCell", for: indexPath) as? StatusTableViewCell else {
                return UITableViewCell()
            }
            cell.delegate = self
            heightStatus = cell.frame.height + 53
            return cell
        case .comment:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ConmentTableViewCell", for: indexPath) as? ConmentTableViewCell else {
                return UITableViewCell()
            }
            let data = arrayComment[indexPath.row]
            cell.commentLabel.text = data
                heightcomment += cell.frame.height
            return cell
        }
    }
    
}

extension PostTableViewCell: CommentDelegate {
    
    func clickCommentButton(_ isCick: Bool) {
        isClickComment = isCick
        if isCick == true {
            arrayComment = arrayComment1
            height = heightStatus + heightcomment
            delegate?.isClickButtonCommnent(isCick,height,heightcomment)
            tableView.reloadData()
            height = 0
            heightcomment = 0
            isClickComment = false
        } else {
            arrayComment = []
            height = heightStatus
            delegate?.isClickButtonCommnent(isCick, height, heightcomment)
            tableView.reloadData()
            height = 0
            heightcomment = 0
            isClickComment = true
        }
    }
    
}
