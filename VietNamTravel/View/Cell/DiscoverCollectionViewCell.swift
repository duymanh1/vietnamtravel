//
//  DiscoverCollectionViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/2/21.
//

import UIKit

class DiscoverCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var placeImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var avatarUserImage: UIImageView!{
        didSet {
            avatarUserImage.layer.cornerRadius = avatarUserImage.frame.height/2
        }
    }
    @IBOutlet weak var nameUserLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpShadow()
    }

    func setUpShadow () {
        placeImage.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        placeImage.layer.cornerRadius = 8
//        shadowView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        shadowView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16).cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        shadowView.layer.shadowOpacity = 1.0
        shadowView.layer.shadowRadius = 4
        shadowView.layer.cornerRadius = 8
    }
}
