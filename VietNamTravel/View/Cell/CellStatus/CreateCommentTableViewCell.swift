//
//  CreateCommentTableViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/2/21.
//

import UIKit

protocol CreateCommentCellDelegate {
    func getProfileUser()
}


class CreateCommentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var commentTextfield: UITextField!
    @IBOutlet weak var avatarImage: UIImageView!{
        didSet {
            avatarImage.layer.cornerRadius = avatarImage.bounds.height/2
            avatarImage.image = UIImage(named: Defaults.getInfoUser().avatarImage)
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
            avatarImage.isUserInteractionEnabled = true
            avatarImage.addGestureRecognizer(tapGestureRecognizer)
        }
    }
    
    var delegate: CreateCommentCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        commentTextfield.layer.cornerRadius = 20
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        delegate?.getProfileUser()
    }
}
