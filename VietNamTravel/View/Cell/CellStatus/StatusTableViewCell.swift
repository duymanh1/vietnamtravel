//
//  StatusTableViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/2/21.
//

import UIKit

protocol CommentDelegate {
    func clickCommentButton(_ isCick: Bool)
}

class StatusTableViewCell: UITableViewCell {
    
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var numberLikeLabel: UILabel!
    @IBOutlet weak var numbercommentLabel: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var contentPostLabel: UILabel!
    
    var isLike = false
    var numberLike = 10
    var delegate: CommentDelegate?
    var isClickButton = false
    var arrayPost = [StatusUser]()
    var idPost: Int?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupLikeImage()
//        setupUI()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupUI() {
        for item in arrayPost {
            if item.idPost == idPost {
                postImage.image = UIImage(named: item.postImage)
                contentPostLabel.text = item.content
            }
        }
    }
    
    @IBAction func clickCommentButton(_ sender: Any) {
        if isClickButton {
            isClickButton = false
        } else {
            isClickButton = true
        }
        delegate?.clickCommentButton(isClickButton)
    }
    
    func setupLikeImage() {
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        likeImage.isUserInteractionEnabled = true
        likeImage.addGestureRecognizer(tapGestureRecognizer)
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
//        let tappedImage = tapGestureRecognizer.view as! UIImageView
        if isLike == false {
            likeImage.image = UIImage(systemName: "hand.thumbsup.fill")
            isLike = true
            numberLike = numberLike + 1
            numberLikeLabel.text = String(numberLike)
        } else {
            likeImage.image = UIImage(systemName: "hand.thumbsup")
            isLike = false
            numberLike = numberLike - 1
            numberLikeLabel.text = String(numberLike)
        }
        
    }
}
