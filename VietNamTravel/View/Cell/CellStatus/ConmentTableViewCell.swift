//
//  ConmentTableViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/2/21.
//

import UIKit

class ConmentTableViewCell: UITableViewCell {

    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var avatarUserImage: UIImageView!{
        didSet {
            avatarUserImage.layer.cornerRadius = avatarUserImage.frame.height/2
        }
    }
    @IBOutlet weak var nameUserLabel: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commentView.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
