//
//  ChatTableViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/24/21.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImage: UIImageView!{
        didSet {
            avatarImage.layer.cornerRadius = avatarImage.frame.height/2
        }
    }
    @IBOutlet weak var messageBodyView: UIView!
    {
        didSet {
            messageBodyView.layer.borderWidth = 1
            messageBodyView.layer.cornerRadius = 10
        }
    }
    @IBOutlet weak var messageBodyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
