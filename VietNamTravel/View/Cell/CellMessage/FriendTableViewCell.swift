//
//  FriendTableViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/24/21.
//

import UIKit

protocol ChooseMesseageDelegate {
    func chooseChat(_ sender: People)
}

class FriendTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var arrayFriend = [People]()
    var delegate: ChooseMesseageDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupUI() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "FriendCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FriendCollectionViewCell")
    }
    
}

extension FriendTableViewCell: UICollectionViewDelegate {
    
}

extension FriendTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayFriend.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FriendCollectionViewCell", for: indexPath) as? FriendCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.avatarImage.image = UIImage(named: arrayFriend[indexPath.row].avatarImage)
        cell.nameLabel.text = arrayFriend[indexPath.row].name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sender = arrayFriend[indexPath.row]
        delegate?.chooseChat(sender)
    }
    
}
