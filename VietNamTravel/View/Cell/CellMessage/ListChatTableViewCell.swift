//
//  ListChatTableViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/24/21.
//

import UIKit


class ListChatTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImage: UIImageView!{
        didSet {
            avatarImage.layer.cornerRadius = avatarImage.bounds.height/2
        }
    }
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
