//
//  MapsTableViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/9/21.
//

import UIKit
import MapKit
import CoreLocation

class MapsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var map: MKMapView!
    
    let manager = CLLocationManager()
    var nameTitle = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
//        map.delegate = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

extension MapsTableViewCell: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            manager.stopUpdatingLocation()
            render(location)
        }
    }
    
    func render(_ location: CLLocation) {
        var coordinate = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(nameTitle) { (placemarks, error) in
            guard let placemarks = placemarks, let location = placemarks.first?.location
            else {
                print("No Location Found")
                return
            }
            coordinate = location.coordinate
            let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
            let regino = MKCoordinateRegion(center: coordinate, span: span)
            self.map.setRegion(regino, animated: true)
            let pin = MKPointAnnotation()
            pin.title = self.nameTitle
            pin.coordinate = coordinate
            self.map.addAnnotation(pin)
        }
    }
}

extension MapsTableViewCell: MKMapViewDelegate {
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        // Don't want to show a custom image if the annotation is the user's location.
//        guard !(annotation is MKUserLocation) else {
//            return nil
//        }
//
//        // Better to make this class property
//        let annotationIdentifier = "AnnotationIdentifier"
//
//        var annotationView: MKAnnotationView?
//        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
//            annotationView = dequeuedAnnotationView
//            annotationView?.annotation = annotation
//        }
//        else {
//            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
//            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
//        }
//
//        if let annotationView = annotationView {
//            // Configure your annotation view here
//            annotationView.canShowCallout = true
//            annotationView.image = UIImage(named: "iconGPS")
//        }
//
//        return annotationView
//    }
}
