//
//  SuggestionsTableViewCell.swift
//  VietNamTravel
//
//  Created by admin on 12/9/21.
//

import UIKit

protocol SuggestionsTableViewCellDelegate {
    func getImagePost()
}

class SuggestionsTableViewCell: UITableViewCell {

    @IBOutlet weak var collectView: UICollectionView!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    var delegate: SuggestionsTableViewCellDelegate?
    var arrayImage = ["hoiAn", "haNoi", "bgLogin"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupUI() {
        
        collectView.delegate = self
        collectView.dataSource = self
        collectView.register(UINib(nibName: "DetailPostAreaCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DetailPostAreaCollectionViewCell")
    }
}

extension SuggestionsTableViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailPostAreaCollectionViewCell", for: indexPath) as? DetailPostAreaCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.postImage.image = UIImage(named: arrayImage[indexPath.row])
        return cell
    }
}

extension SuggestionsTableViewCell: UICollectionViewDelegate {
}
