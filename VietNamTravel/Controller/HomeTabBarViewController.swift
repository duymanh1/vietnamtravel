//
//  HomeTabBarViewController.swift
//  VietNamTravel
//
//  Created by admin on 12/9/21.
//

import UIKit

class HomeTabBarViewController: UITabBarController {
    @IBOutlet weak var tabbartint: UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillLayoutSubviews() {
        var tabFrame = self.tabbartint.frame
        // - 40 is editable , the default value is 49 px, below lowers the tabbar and above increases the tab bar size
        self.tabBar.frame.size.height = 5
        self.tabBar.frame.origin.y = self.view.frame.size.height - 20
//        self.tabbartint.frame = tabFrame
    }


}
