//
//  LoginViewController.swift
//  VietNamTravel
//
//  Created by admin on 12/8/21.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
    
    @IBOutlet weak var introduceView: UIView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var facebookView: UIView!
    @IBOutlet weak var googleView: UIView!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    var arrayUser = [User]()
    var user = User()
    var backIsHidden = false
    var emailUser: String?
    var isLogin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getUserFirebase()
    }
    
    @IBAction func clickBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupUI() {
        backButton.isHidden = backIsHidden
        self.tabBarController?.tabBar.isHidden = !backIsHidden
        self.navigationController?.navigationBar.isHidden = true
        loginView.backgroundColor = UIColor(white: 0.3, alpha: 0.4)
        loginButton.layer.cornerRadius = 17.5
        facebookView.layer.cornerRadius = 17.5
        googleView.layer.cornerRadius = 17.5
        emailTextField.layer.cornerRadius = 8
        passwordTextField.layer.cornerRadius = 8
        createAccountButton.layer.cornerRadius = 17.5
        emailTextField.layer.masksToBounds = true
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.borderColor = .init(red: 1, green: 1, blue: 1, alpha: 1)
        passwordTextField.layer.masksToBounds = true
        passwordTextField.layer.borderWidth = 1
        passwordTextField.layer.borderColor = .init(red: 1, green: 1, blue: 1, alpha: 1)
    }
    func showAlert(_ msg: String){
        let alertController = UIAlertController(title: "Error", message: msg, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(alertAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func clickLoginButton(_ sender: Any) {
        Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            if error != nil {
                debugPrint(error)
                self.showAlert(error?.localizedDescription ?? "")
            } else {
//                self.emailUser = self.emailTextField.text
//                self.getUserFirebase()
//                if Defaults.getInfoUser().email != "" {
//                    self.navigationController?.popViewController(animated: true)
//                }
                for user in self.arrayUser {
                    if user.email == self.emailTextField.text {
                        Defaults.save(user.fullName, address: user.address, avatarImage: user.avatarImage, gender: user.gender, birthday: user.birthday, email: user.email, phoneNumber: "0968140814")
//                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//                        self.navigationController?.pushViewController(vc, animated: true)
                        self.navigationController?.popViewController(animated: true)
                        return
                    }
                }
                
            }
        }
    }
    
    func getUserFirebase() {
        
        let userDB = Database.database().reference().child("User")

        userDB.observe(.childAdded) {(snapShot) in
            let value = snapShot.value as! Dictionary<String, String>
            let fullname = value["Fullname"]!
            let avatarUser = value["AvatarImage"]!
            let gender = value["Gender"]!
            let birthday = value["Birthday"]!
            let address = value["Address"]!
            let email = value["Email"]!
            var user: User?
           
            self.arrayUser.append(User(fullName: fullname, avatarImage: avatarUser, gender: gender, birthday: birthday, address: address, email: email))
        }
    }
    
    @IBAction func clickCreateAccountButton(_ sender: Any)  {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController 
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
