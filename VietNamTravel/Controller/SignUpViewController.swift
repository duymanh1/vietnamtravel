//
//  SignUpViewController.swift
//  VietNamTravel
//
//  Created by admin on 12/9/21.
//

import UIKit
import Toast_Swift
import Firebase
import FirebaseDatabase

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var sigupButton: UIButton!
    @IBOutlet weak var fullnameTextField: UITextField!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var birthdayDatePicker: UIDatePicker!
    @IBOutlet weak var genderSegmentedControl: UISegmentedControl!
    
    var gender: String?
    var email: String?
    var avatar: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        backgroundView.backgroundColor = UIColor(white: 0.3, alpha: 0.4)
        dateView.layer.cornerRadius = 6
        sigupButton.layer.cornerRadius = 17.5
        dateView.isHidden = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.textFieldEditing))
        self.birthdayTextField.addGestureRecognizer(tap)
        gender = (genderSegmentedControl.titleForSegment(at: 0))
    }
    
    
    
    @objc func textFieldEditing() {
        self.birthdayTextField.resignFirstResponder()
        dateView.isHidden = false
        
    }
    
    @IBAction func clickDoneButton(_ sender: Any) {
        let date = DateFormatter()
        date.dateFormat = "dd-MM-yyyy"
        birthdayTextField.text = date.string(from: birthdayDatePicker.date)
        dateView.isHidden = true
    }
    
    @IBAction func clickCancelButton(_ sender: Any) {
        dateView.isHidden = true
    }
    
    
    @IBAction func clickSigupButton(_ sender: Any) {
        if birthdayTextField.text == "" {
            self.view.makeToast("Choose your birthday")
            return
        }
        if fullnameTextField.text == "" {
            self.view.makeToast("Enter your full name")
            return
        }
        if addressTextField.text == "" {
            self.view.makeToast("Enter your address")
            return
        }
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(vc, animated: true)
        print(fullnameTextField.text , "/n", birthdayTextField.text, "/n", addressTextField.text, "/n" , gender )
        avatar = "hoiAn"
        saveUserFisebase()
        self.view.makeToast("Register Success")
        
    }
    
    func saveUserFisebase() {
        let userDB = Database.database().reference().child("User")
        let userDict = ["Fullname" : fullnameTextField.text, "AvatarImage" : avatar, "Gender" : gender, "Birthday" : birthdayTextField.text, "Address" : addressTextField.text, "Email" : email]
        userDB.childByAutoId().setValue(userDict) {(error, ref) in
            if error != nil {
                debugPrint(error)
            }else {
                debugPrint("Sigup successfully")
            }
        }
    }
    
    
    
    @IBAction func chooseSexSegmentedControl(_ sender: Any) {
        //    sexTextField.text = sexSegmentedControl.titleForSegment(at: 0)
        switch genderSegmentedControl.selectedSegmentIndex {
        case 0:
            gender = genderSegmentedControl.titleForSegment(at: 0)
        case 1:
            gender = genderSegmentedControl.titleForSegment(at: 1)
        default:
            break;
        }
    }
    
    @IBAction func clickBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
