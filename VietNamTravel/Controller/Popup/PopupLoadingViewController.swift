//
//  PopupLoadingViewController.swift
//  VietNamTravel
//
//  Created by admin on 12/31/21.
//

import UIKit

class PopupLoadingViewController: UIViewController {

    @IBOutlet weak var loadingView: UIView!{
        didSet {
            loadingView.layer.cornerRadius = 8
        }
    }
    @IBOutlet weak var bgView: UIView!{
        didSet {
            bgView.backgroundColor = UIColor(white: 0.1, alpha: 0.8)
        }
    }
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!{
        didSet {
            loadingActivity.startAnimating()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
