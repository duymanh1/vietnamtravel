//
//  PopupMapsViewController.swift
//  VietNamTravel
//
//  Created by admin on 12/13/21.
//

import UIKit
import MapKit
import CoreLocation


class PopupMapViewController: UIViewController {
    
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    
    var locationManger = CLLocationManager()
    let geoCoder = CLGeocoder()
    var nameTitle = ""
    var distance = 0.0
    var coordinate0 = CLLocation(latitude: 0.0, longitude: 0.0)
    var coordinate1 = CLLocation(latitude: 0.0, longitude: 0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        map.layer.cornerRadius = 8
        popupView.backgroundColor = UIColor(white: 0.3, alpha: 0.7)
        locationManger.desiredAccuracy = kCLLocationAccuracyBest
        locationManger.requestAlwaysAuthorization()
        locationManger.requestWhenInUseAuthorization()
        locationManger.startUpdatingLocation()
        print("--------", locationManger.location?.coordinate)
        self.coordinate0 = locationManger.location!
        map.delegate = self
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(tapGestureRecognizer:)))
        dismissView.isUserInteractionEnabled = true
        dismissView.addGestureRecognizer(tapGestureRecognizer)
        distanceLabel.text = ""
        
    }
    
    @objc func viewTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        dismiss(animated: true)
    }
    
    @IBAction func clickGotoButton(_ sender: Any) {
        getAddress()
    }
    
    func getAddress() {
        geoCoder.geocodeAddressString(nameTitle) { (placemarks, error) in
            guard let placemarks = placemarks, let location = placemarks.first?.location
            else {
                print("No Location Found")
                return
            }
            print("++++++++++++", location)
            self.coordinate1 = location
            self.mapThis(destinationCord: location.coordinate)
            let distanceInMeters = self.coordinate0.distance(from: self.coordinate1)
            let km = distanceInMeters / 500.0
            self.distanceLabel.text = String(format: "%.1f", km) + "km"
            
            let pin = MKPointAnnotation()
            pin.title = self.nameTitle
            pin.coordinate = location.coordinate
            self.map.addAnnotation(pin)
        }
        
    }
    
    func mapThis(destinationCord : CLLocationCoordinate2D) {
        guard let souceCordinate = (locationManger.location?.coordinate) else {
            return
        }
        
        let soucePlaceMark = MKPlacemark(coordinate: souceCordinate)
        let destPlaceMark = MKPlacemark(coordinate: destinationCord)
        
        let sourceItem = MKMapItem(placemark: soucePlaceMark)
        let destItem = MKMapItem(placemark: destPlaceMark)
        
        let destinationRequest = MKDirections.Request()
        destinationRequest.source = sourceItem
        destinationRequest.destination = destItem
        destinationRequest.transportType = .automobile
        destinationRequest.requestsAlternateRoutes = true
        
        let directions = MKDirections(request: destinationRequest)
        directions.calculate { (response, error) in
            guard let response = response else {
                if let error = error {
                    print("Something is wrong :(")
                }
                return
            }
            let route = response.routes[0]
            self.map.addOverlay(route.polyline)
            self.map.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
        }
    }
    
    
}

extension PopupMapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let render = MKPolylineRenderer(overlay: overlay as! MKPolyline)
        render.strokeColor = .blue
        return render
    }
    
    
}
