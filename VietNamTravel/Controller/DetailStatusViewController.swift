//
//  DetailStatusViewController.swift
//  VietNamTravel
//
//  Created by admin on 12/2/21.
//

import UIKit

enum statusTable: Int {
    case status
    case comment
    
    init(index: Int) {
        switch index {
        case 0:
            self = .status
        case 1:
            self = .comment
        default:
            self = .comment
        }
    }
}


class DetailStatusViewController: UIViewController {
    
    @IBOutlet weak var avatarPosterImage: UIImageView!{
        didSet {
            avatarPosterImage.layer.cornerRadius = avatarPosterImage.frame.height/2
        }
    }
    @IBOutlet weak var namePosterLabel: UIButton!
    @IBOutlet weak var statusTableView: UITableView!
    @IBOutlet weak var createCommentTextField: UITextField!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var createCommentButton: UIButton!
    @IBOutlet weak var avatarUserImage: UIImageView!{
        didSet {
            avatarUserImage.layer.cornerRadius = avatarUserImage.frame.height/2
            if Defaults.getInfoUser().email != "" {
                avatarUserImage.image = UIImage(named: Defaults.getInfoUser().avatarImage)
            } else {
                avatarUserImage.image = UIImage(systemName: "person.circle.fill")
            }
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
            avatarUserImage.isUserInteractionEnabled = true
            avatarUserImage.addGestureRecognizer(tapGestureRecognizer)
        }
    }
    
    var sections: [statusTable] = []
    var arrayComment = [Comment]()
    var arrayComment2 = [Comment]()
    var arrayComment1 = [Comment]()
    var post: StatusUser?
    var arrayPost = [StatusUser]()
    var idPost = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sections.removeAll()
        sections.append(contentsOf: [.status, .comment])
        for item in arrayPost {
            if item.idPost == idPost {
                post = item
                
            }
        }
        avatarPosterImage.image = UIImage(named: post?.Poster.avatarImage ?? "")
        namePosterLabel.setTitle(post?.Poster.name, for: .normal)
//        arrayComment = ["Good", "Kẻ lữ khách là những con người mộng mơ dám biến những chuyến hành trình trong trí tưởng tượng thành sự thật", "Very Good", "Wonderful"]
        arrayComment2 = [Comment(user: People(avatar: "hoiAn", name: "Bich Diep"), content: "Good", idPost: 2),
                         Comment(user: People(avatar: "haNoi", name: "Duy Manh"), content: "Good", idPost: 2),
                         Comment(user: People(avatar: "hoiAn", name: "Bich Diep"), content: "Good", idPost: 3),
                         Comment(user: People(avatar: "hoiAn", name: "Bich Diep"), content: "Good", idPost: 2),
                         Comment(user: People(avatar: "haNoi", name: "Duy Manh"), content: "Good", idPost: 1),
                         Comment(user: People(avatar: "hoiAn", name: "Bich Diep"), content: "Good", idPost: 2)]
        for item in arrayComment2 {
            if item.idPost == idPost {
                arrayComment.append(item)
            }
        }

        arrayComment1 = Array(arrayComment.reversed())
        setUpStatusTableView()
        createCommentTextField.delegate = self
        createCommentButton.isEnabled = false
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange), name: UIApplication.keyboardWillChangeFrameNotification, object: nil)
    }
    
    //    override func viewWillAppear(_ animated: Bool) {
    //        super.viewWillAppear(animated)
    //        if createCommentTextField.text == "" {
    //            createCommentButton.isEnabled = false
    //        } else {
    //            createCommentButton.isEnabled = true
    //        }
    //    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        createCommentTextField.resignFirstResponder()
    }
    
    @objc func imageTapped() {
        tabBarController?.selectedIndex = 3
    }
    
    @objc func keyboardWillChange(notification: NSNotification) {
        
        let duration = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
        let curFrame = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let targetFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let deltaY = targetFrame.origin.y - curFrame.origin.y
        print("deltaY",deltaY)
        
        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIView.KeyframeAnimationOptions(rawValue: curve), animations: {
            if deltaY == -346.0 {
                self.commentView.frame.origin.y+=deltaY + 85
            } else {
                self.commentView.frame.origin.y+=deltaY
            }
            // Here You Can Change UIView To UITextField
        },completion: nil)
    }
    
    
    @IBAction func clickBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUpStatusTableView() {
        statusTableView.delegate = self
        statusTableView.dataSource = self
        statusTableView.register(UINib(nibName: "StatusTableViewCell", bundle: nil), forCellReuseIdentifier: "StatusTableViewCell")
        statusTableView.register(UINib(nibName: "ConmentTableViewCell", bundle: nil), forCellReuseIdentifier: "ConmentTableViewCell")
        statusTableView.separatorStyle = .none
    }
    
    @IBAction func clickcommentButton(_ sender: Any) {
        if createCommentTextField.text != "" {
            arrayComment2.append(Comment(user: People(avatar: Defaults.getInfoUser().avatarImage, name: Defaults.getInfoUser().name), content: createCommentTextField.text ?? "", idPost: idPost))
            arrayComment.append(Comment(user: People(avatar: Defaults.getInfoUser().avatarImage, name: Defaults.getInfoUser().name), content: createCommentTextField.text ?? "", idPost: idPost))
            arrayComment1 = Array(arrayComment.reversed())
            statusTableView.reloadData()
            createCommentTextField.text = ""
            createCommentTextField.isEnabled = false
            createCommentButton.isEnabled = false
            createCommentTextField.isEnabled = true
        }
    }
    
}

extension DetailStatusViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = sections[section]
        switch section {
        case .status:
            return 1
        case .comment:
            return arrayComment1.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sections[indexPath.section]
        switch section {
        case .status:
            guard let cell = statusTableView.dequeueReusableCell(withIdentifier: "StatusTableViewCell", for: indexPath) as? StatusTableViewCell else {
                return UITableViewCell()
            }
            cell.numbercommentLabel.text = String(arrayComment1.count)
            cell.postImage.image = UIImage(named: post?.postImage ?? "")
            cell.contentPostLabel.text = post?.content
        return cell
        case .comment:
        guard let cell = statusTableView.dequeueReusableCell(withIdentifier: "ConmentTableViewCell", for: indexPath) as? ConmentTableViewCell else {
            return UITableViewCell()
        }
        let data = arrayComment1[indexPath.row]
            cell.commentLabel.text = data.content
            cell.nameUserLabel.setTitle(data.user.name, for: .normal)
            cell.avatarUserImage.image = UIImage(named: data.user.avatarImage)
        return cell
    }
}
}

extension DetailStatusViewController: UITableViewDelegate {
    
}

extension DetailStatusViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        createCommentTextField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if createCommentTextField.text == "" {
            createCommentButton.isEnabled = true
        } else {
            createCommentButton.isEnabled = false
        }
    }
}

