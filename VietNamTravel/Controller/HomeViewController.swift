//
//  HomeViewController.swift
//  VietNamTravel
//
//  Created by admin on 12/2/21.
//

import UIKit
protocol SentArrayPostDelegate {
    func getArrayPost(_ arrayPost: [StatusUser])
}

class HomeViewController: UIViewController {
    
    @IBOutlet weak var avartarUserImage: UIImageView! {
        didSet {
            avartarUserImage.layer.cornerRadius = avartarUserImage.frame.height/2
        }
    }
    @IBOutlet weak var createStatusButton: UIButton!
    @IBOutlet weak var discoverCollectionView: UICollectionView!
    @IBOutlet weak var bannerImage: UIImageView!
    
    
    var arrayStatus = [StatusUser]()
    var delegate: SentArrayPostDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
//        arrayStatus = [StatusUser(tourImage: "hoiAn", tittle: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.", detail: "David Đức", like: 1),
//                       StatusUser(tourImage: "hoiAn", tittle: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.", detail: "David Đức", like: 1),
//                       StatusUser(tourImage: "hoiAn", tittle: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.", detail: "David Đức", like: 1),
//                       StatusUser(tourImage: "hoiAn", tittle: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.", detail: "David Đức", like: 1),
//                       StatusUser(tourImage: "hoiAn", tittle: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.", detail: "David Đức", like: 1),
//                       StatusUser(tourImage: "hoiAn", tittle: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.", detail: "David Đức", like: 1),
//                       StatusUser(tourImage: "hoiAn", tittle: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.", detail: "David Đức", like: 1),
//                       StatusUser(tourImage: "hoiAn", tittle: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.", detail: "David Đức", like: 1),
//                       StatusUser(tourImage: "hoiAn", tittle: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.", detail: "David Đức", like: 1)]
        arrayStatus = [StatusUser(postImage: "hoiAn", content: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.1", Poster: People(avatar: "hoiAn", name: "David Đức"), like: 0, idPost: 1),
                       StatusUser(postImage: "hoiAn", content: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.2", Poster: People(avatar: "hoiAn", name: "David Đức"), like: 0, idPost: 2),
                       StatusUser(postImage: "haNoi", content: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.3", Poster: People(avatar: "hoiAn", name: "David Đức"), like: 0, idPost: 3),
                       StatusUser(postImage: "hoiAn", content: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.4", Poster: People(avatar: "hoiAn", name: "David Đức"), like: 0, idPost: 4),
                       StatusUser(postImage: "haNoi", content: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.5", Poster: People(avatar: "hoiAn", name: "David Đức"), like: 0, idPost: 5),
                       StatusUser(postImage: "hoiAn", content: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.6", Poster: People(avatar: "hoiAn", name: "David Đức"), like: 0, idPost: 6),
                       StatusUser(postImage: "hoiAn", content: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.7", Poster: People(avatar: "hoiAn", name: "David Đức"), like: 0, idPost: 7),
                       StatusUser(postImage: "haNoi", content: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.8", Poster: People(avatar: "hoiAn", name: "David Đức"), like: 0, idPost: 8),
                       StatusUser(postImage: "hoiAn", content: "Hội An Phố Cổ giản dị và cổ kính đúng như tên gọi.9", Poster: People(avatar: "hoiAn", name: "David Đức"), like: 0, idPost: 9)]
        delegate?.getArrayPost(arrayStatus)
        setupCollectionView()
        createStatusButton.layer.cornerRadius = 6
        setupAvatarUser()
        let screenWidth = UIScreen.main.bounds.width - 10
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: screenWidth/2-20, height: (screenWidth/2)*5/4)
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 5
        discoverCollectionView!.collectionViewLayout = layout
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Defaults.getInfoUser().email != "" {
            avartarUserImage.image = UIImage(named: Defaults.getInfoUser().avatarImage)
        } else {
            avartarUserImage.image = UIImage(systemName: "person.circle.fill")
        }
    }
    
    func bannerNotification() {
        //        let container = UIView()
        //        let image = UIImageView()
        //        let label = UILabel()
        //        container.frame = CGRect(x: 0, y:-100, width: self.view.frame.size.width, height: 100)
        //        container.backgroundColor = .blue
        //        image.frame = CGRect(x: 15, y: 60, width: 30, height: 30)
        //        image.image = UIImage(named: "passport")
        //        label.frame = CGRect(x: image.bounds.maxX + 35, y: 40, width: container.frame.size.width - 100, height: 50)
        //        label.numberOfLines = 0
        //        label.font = UIFont(name:"Helvetica Neue", size: 15)
        //        label.text = text
        //        container.addSubview(image)
        //        container.addSubview(label)
        //        UIApplication.shared.windows[0].addSubview(bannerImage)
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
                self.bannerImage.frame = CGRect(x:0, y: 0, width: self.view.frame.size.width, height: 100)
                
            }) { (finished) in
                UIView.animate(withDuration: 0.4,delay: 2.0, options: .curveLinear, animations: {
                    self.bannerImage.frame = CGRect(x:0, y: -100, width: self.view.frame.size.width, height: 100)
                })
            }
        }
    }
    
    func setupAvatarUser() {
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped   ))
        avartarUserImage.isUserInteractionEnabled = true
        avartarUserImage.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if Defaults.getInfoUser().email == "" {
            let viewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        } else {
//            let viewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AccountViewController") as! AccountViewController
//            self.navigationController?.pushViewController(viewcontroller, animated: true)
            tabBarController?.selectedIndex = 3
        }
        
    }
    
    func setupCollectionView() {
        discoverCollectionView.delegate = self
        discoverCollectionView.dataSource = self
        discoverCollectionView.register(UINib(nibName: "DiscoverCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cellDiscover")
    }
    
    @IBAction func clickCreateStatusButton(_ sender: Any) {
    }
    
}

extension HomeViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayStatus.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = discoverCollectionView.dequeueReusableCell(withReuseIdentifier: "cellDiscover", for: indexPath) as! DiscoverCollectionViewCell
        let data = arrayStatus[indexPath.row]
        cell.placeImage.image = UIImage(named: data.postImage)
        cell.titleLabel.text = data.content
        cell.nameUserLabel.text = data.Poster.name
        cell.avatarUserImage.image = UIImage(named: data.Poster.avatarImage)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 195.0, height: 310.0)
    }
    
}

extension HomeViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailStatus") as! DetailStatusViewController
        let data = arrayStatus[indexPath.row]
        viewcontroller.idPost = data.idPost
        viewcontroller.arrayPost = arrayStatus
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
}
