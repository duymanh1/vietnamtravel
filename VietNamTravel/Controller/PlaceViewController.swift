//
//  PlaceViewController.swift
//  VietNamTravel
//
//  Created by admin on 12/6/21.
//

import UIKit

enum placeTable: Int {
    case history
    case place
    case travelogue
    
    init(index: Int) {
        switch index {
        case 0:
            self = .history
        case 1:
            self = .place
        case 2:
            self = .travelogue
        default:
            self = .travelogue
        }
    }
}

class PlaceViewController: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var placeTableView: UITableView!
    @IBOutlet private weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundHeaderImage: UIImageView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var sections: [placeTable] = []
    private let maxHeaderHeight: CGFloat = 178.0
    private let minHeaderHeight: CGFloat = 75.0
    private var previousScrollOffset: CGFloat = 0.0
    var i = 0
    var imageArr = ["BackgroundLogin", "background", "background2"]
    var namePlaceHistory = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        headerHeightConstraint.constant = maxHeaderHeight
        self.updateHeader()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupUI() {
        sections.removeAll()
        sections.append(contentsOf: [.history, .place, .travelogue])
        Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(imageChange), userInfo: nil, repeats: true)
        placeTableView.delegate = self
        placeTableView.dataSource = self
        placeTableView.register(UINib(nibName: "HistorySearchTableViewCell", bundle: nil), forCellReuseIdentifier: "HistorySearchTableViewCell")
        placeTableView.register(UINib(nibName: "PlaceTableViewCell", bundle: nil), forCellReuseIdentifier: "PlaceTableViewCell")
        placeTableView.register(UINib(nibName: "TravelogueTableViewCell", bundle: nil), forCellReuseIdentifier: "TravelogueTableViewCell")
        navigationController?.navigationBar.isHidden = true
        searchBar.layer.cornerRadius = 5
        searchBar.backgroundColor = .clear
        searchBar.searchTextField.font = UIFont(name: "Arial", size: 12)
    }
    
    @objc func imageChange(){
        self.backgroundHeaderImage.image = UIImage(named: imageArr[i])
        if i < imageArr.count-1{
            i += 1
        }
        else{
            i = 0
        }
    }
    // MARK: Methods
    private func canAnimateHeader(_ scrollView: UIScrollView) -> Bool {
        // Calculate height của scroll view khi header view bị collapse đến min height
        let scrollViewMaxHeight = scrollView.frame.height + headerHeightConstraint.constant - minHeaderHeight
        // Đảm bảo khi header bị collapse đến min height thì scroll view vẫn scroll được
        return scrollView.contentSize.height > scrollViewMaxHeight
    }
    
    private func setScrollPosition(_ position: CGFloat) {
        placeTableView.contentOffset = CGPoint(x: placeTableView.contentOffset.x, y: position)
    }
    
    private func scrollViewDidStopScrolling() {
        let range = maxHeaderHeight - minHeaderHeight
        let midPoint = minHeaderHeight + range / 2
        
        if headerHeightConstraint.constant > midPoint {
            // Expand header
            expandHeader()
        } else {
            // Collapse header
            collapseHeader()
        }
    }
    
    private func collapseHeader() {
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.headerHeightConstraint.constant = self.minHeaderHeight
            self.updateHeader()
            self.view.layoutIfNeeded()
        })
    }
    
    private func expandHeader() {
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.headerHeightConstraint.constant = self.maxHeaderHeight
            self.updateHeader()
            self.view.layoutIfNeeded()
        })
    }
    
    private func updateHeader() {
        // Tính khoảng cách giữa 2 value max và min height
        let range = maxHeaderHeight - minHeaderHeight
        // Tính khoảng offset hiện tại với min height
        let openAmount = headerHeightConstraint.constant - minHeaderHeight
        // Tính tỉ lệ phần trăm để animate, thay đổi UI element
        let percentage = openAmount / range
        // Tính constant của trailing constraint cần thay đổi
        //        let trailingRange = view.frame.width - chatButton.frame.minX
        
        // Animate UI theo tỉ lệ tính được
        //        searchTextFieldTrailingConstraint.constant = trailingRange * (1.0 - percentage) + 8
        //        logoImageView.alpha = percentage
    }
}

extension PlaceViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = sections[indexPath.section]
        switch section {
        case .history:
            return 70
        case .place:
            return 270
        case .travelogue:
            return 170
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollDiff = scrollView.contentOffset.y - previousScrollOffset
        
        // Điểm giới hạn trên cùng của scroll view
        let absoluteTop: CGFloat = 0.0
        // Điểm giới hạn dưới cùng của scroll view
        let absoluteBottom: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height
        
        let isScrollingDown = scrollDiff > 0 && scrollView.contentOffset.y > absoluteTop
        let isScrollingUp = scrollDiff < 0 && scrollView.contentOffset.y < absoluteBottom
        
        guard canAnimateHeader(scrollView) else {
            return
        }
        
        // Implement logic để animate header
        var newHeight = headerHeightConstraint.constant
        if isScrollingDown {
            newHeight = max(minHeaderHeight, headerHeightConstraint.constant - abs(scrollDiff))
        } else if isScrollingUp {
            newHeight = min(maxHeaderHeight, headerHeightConstraint.constant + abs(scrollDiff))
        }
        
        if newHeight != self.headerHeightConstraint.constant {
            headerHeightConstraint.constant = newHeight
            updateHeader()
            setScrollPosition(previousScrollOffset)
        }
        
        previousScrollOffset = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // Kết thúc scroll
        scrollViewDidStopScrolling()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            // Kết thúc scroll
            scrollViewDidStopScrolling()
        }
    }
    
}

extension PlaceViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = sections[section]
        switch section {
        case .history:
            return 1
        case .place:
            return 1
        case .travelogue:
            return 4
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sections[indexPath.section]
        switch section {
        case .history:
            guard let cell =  placeTableView.dequeueReusableCell(withIdentifier: "HistorySearchTableViewCell", for: indexPath) as? HistorySearchTableViewCell else {
                return UITableViewCell()
            }
            cell.namePlace = namePlaceHistory
            return cell
        case .place:
            guard let cell =  placeTableView.dequeueReusableCell(withIdentifier: "PlaceTableViewCell", for: indexPath) as? PlaceTableViewCell else {
                return UITableViewCell()
            }
            cell.delegate = self
            return cell
        case .travelogue:
            guard let cell =  placeTableView.dequeueReusableCell(withIdentifier: "TravelogueTableViewCell", for: indexPath) as? TravelogueTableViewCell else {
                return UITableViewCell()
            }
            return cell
        }
    }
    
}

extension PlaceViewController: ChooseAreaDelegate {
    func selectArea(_ namePlace: String) {
        namePlaceHistory = namePlace
        let viewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailPlaceViewController") as! DetailPlaceViewController
//        viewcontroller.nameStatus = arrayStatus[indexPath.row].tittle
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func selectArea() {
        let viewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailPlaceViewController") as! DetailPlaceViewController
//        viewcontroller.nameStatus = arrayStatus[indexPath.row].tittle
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
}
