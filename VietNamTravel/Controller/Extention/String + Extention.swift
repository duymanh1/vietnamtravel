//
//  String + Extention.swift
//  VietNamTravel
//
//  Created by admin on 12/27/21.
//

import Foundation
import Firebase

extension String {
    func validateEmail() -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: self)
    }
}

struct Defaults {
    
    static let (nameKey, addressKey, avatarImageKey, genderKey, birthdayKey, emailKey, phoneNumberKey) = ("name", "address", "avatarImage", "gender", "birthday", "email", "phoneNumber")
    static let userSessionKey = "com.save.usersession"
    private static let userDefault = UserDefaults.standard
    
    /**
       Nó được sử dụng để lấy ra và gán giá trị người dùng vào UserDefaults
     */
    struct UserDetails {
        let name: String
        let address: String
        let avatarImage: String
        let gender: String
        let birthday: String
        let email: String
        let phoneNumber: String
        
        init(_ json: [String: String]) {
            self.name = json[nameKey] ?? ""
            self.address = json[addressKey] ?? ""
            self.avatarImage = json[avatarImageKey] ?? ""
            self.gender = json[genderKey] ?? ""
            self.birthday = json[birthdayKey] ?? ""
            self.email = json[emailKey] ?? ""
            self.phoneNumber = json[phoneNumberKey] ?? ""
        }
    }
    
    /**
     - Lưu chi tiết người dùng
     - Inputs - name `String` & address `String`
     */
    static func save(_ name: String, address: String, avatarImage: String, gender: String, birthday: String, email: String, phoneNumber: String){
        userDefault.set([nameKey: name, addressKey: address, avatarImageKey: avatarImage, genderKey: gender, birthdayKey: birthday, emailKey: email, phoneNumberKey: phoneNumber ],
                        forKey: userSessionKey)
    }
    
    /**
     - Tìm nạp các giá trị thông qua Model `UserDetails`
     - Output - `UserDetails` model
     */
    static func getInfoUser()-> UserDetails {
        return UserDetails((userDefault.value(forKey: userSessionKey) as? [String: String]) ?? [:])
    }
    
    /**
        - Xoá chi tiết người dùng trong UserDefault qua key "com.save.usersession"
     */
    static func clearUserData(){
        userDefault.removeObject(forKey: userSessionKey)
    }
}


struct FirebaseData {
    func getUser() -> [User] {
        var arrayUser = [User]()
        let userDB = Database.database().reference().child("User")
        userDB.observe(.childAdded) {(snapShot) in
            let value = snapShot.value as! Dictionary<String, String>
            let fullname = value["Fullname"]!
            let avatarUser = value["AvatarImage"]!
            let gender = value["Gender"]!
            let birthday = value["Birthday"]!
            let address = value["Address"]!
            let email = value["Email"]!
            var user: User?
            user?.fullName = fullname
            user?.avatarImage = avatarUser
            user?.gender = gender
            user?.birthday = birthday
            user?.address = address
            user?.email = email
            arrayUser.append(user ?? User())
        }
        return arrayUser
    }
}
