//
//  MessageViewController.swift
//  VietNamTravel
//
//  Created by admin on 12/23/21.
//

import UIKit
import Firebase

enum MessageSection: Int {
    case friend
    case chat
    
    init(index: Int) {
        switch index {
        case 0:
            self = .friend
        case 1:
            self = .chat
        default:
            self = .chat
        }
    }
}

class MessageViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var avatarUserImage: UIImageView!{
        didSet {
            avatarUserImage.layer.cornerRadius = avatarUserImage.bounds.height/2
            if Defaults.getInfoUser().email != "" {
                avatarUserImage.image = UIImage(named: Defaults.getInfoUser().avatarImage)
            } else {
                avatarUserImage.image = UIImage(systemName: "person.circle.fill")
            }
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
            avatarUserImage.isUserInteractionEnabled = true
            avatarUserImage.addGestureRecognizer(tapGestureRecognizer)
        }
    }
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var headerHeightContraint: NSLayoutConstraint!
    
    var array = [Message]()
    var arrayUser = [User]()
    var sections: [MessageSection] = []
    var filteredData = [""]
    var arrayStringTittle = [""]
    var filteredTour = [Message]()
    var arrayFriend = [People]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        self.navigationController?.navigationBar.isHidden = true
        searchBar.delegate = self
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "PopupLoadingViewController") as? PopupLoadingViewController else { return }
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            vc.loadingActivity.stopAnimating()
            self.dismiss(animated: false, completion: nil)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupUI()
        getUserFirebase()
        getMessage()
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        tabBarController?.selectedIndex = 3
    }
    
    func setupUI() {
        sections.removeAll()
        sections.append(contentsOf: [.friend, .chat])
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "FriendTableViewCell", bundle: nil), forCellReuseIdentifier: "FriendTableViewCell")
        tableView.register(UINib(nibName: "ListChatTableViewCell", bundle: nil), forCellReuseIdentifier: "ListChatTableViewCell")
        
        
//        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
//
//        alert.view.tintColor = .black
////        CGRectMake(10, 5, 50, 50)
//        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
//        loadingIndicator.hidesWhenStopped = true
//        loadingIndicator.style = UIActivityIndicatorView.Style.gray
//        loadingIndicator.startAnimating();
//
//        alert.view.addSubview(loadingIndicator)
//        present(alert, animated: true, completion: nil)
        
    }
    
    func getMessage() {
        
        array.removeAll()
        arrayStringTittle.removeAll()
        filteredTour.removeAll()
        filteredData.removeAll()
        let messageDB = Database.database().reference().child("Messages")
        messageDB.observe(.childAdded) {(snapShot) in
            let value = snapShot.value as! Dictionary<String, String>
            let text = value["MessageBody"]!
            let sender = value["Sender"]!
            let user = value["User"]!
            if (sender == Defaults.getInfoUser().name)  {
                for item in self.arrayUser {
                    if item.fullName == user {
                        var temp = false
                        for msg in self.array {
                            if item.fullName == msg.sender?.name {
                                temp = true
                            }
                        }
                        if temp == false {
                            self.array.append(Message(user: People(avatar: Defaults.getInfoUser().avatarImage, name: Defaults.getInfoUser().name), sender: People(avatar: item.avatarImage, name: item.fullName), message: text))
                            
                                self.arrayStringTittle.append(item.fullName)
                            self.filteredData = self.arrayStringTittle
                            self.filteredTour = self.array
                            self.tableView.reloadData()
                            
                        }
                    }
                }
            }
            self.tableView.reloadData()
        }
    }
    
    func getUserFirebase() {
        arrayUser.removeAll()
        arrayFriend.removeAll()
        let userDB = Database.database().reference().child("User")
        userDB.observe(.childAdded) {(snapShot) in
            let value = snapShot.value as! Dictionary<String, String>
            let fullname = value["Fullname"]!
            let avatarUser = value["AvatarImage"]!
            let gender = value["Gender"]!
            let birthday = value["Birthday"]!
            let address = value["Address"]!
            let email = value["Email"]!
            self.arrayUser.append(User(fullName: fullname, avatarImage: avatarUser, gender: gender, birthday: birthday, address: address, email: email))
            if fullname != Defaults.getInfoUser().name {
                self.arrayFriend.append(People(avatar: avatarUser, name: fullname))
//                self.tableView.reloadData()
            }
            self.tableView.reloadData()
        }
    }
}

extension MessageViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = sections[indexPath.section]
        switch section {
        case .friend:
            return 100
        case .chat:
            return 70
        }
    }
}

extension MessageViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = sections[section]
        switch section {
        case .friend:
            return 1
        case .chat:
            return filteredTour.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sections[indexPath.section]
        switch section {
        case .friend:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FriendTableViewCell", for: indexPath) as? FriendTableViewCell else {
                return UITableViewCell()
            }
            cell.arrayFriend = arrayFriend
            cell.collectionView.reloadData()
            cell.delegate = self
            return cell
        case .chat:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListChatTableViewCell", for: indexPath) as? ListChatTableViewCell else {
                return UITableViewCell()
            }
            let data = filteredTour[indexPath.row]
            cell.avatarImage.image = UIImage(named: data.sender?.avatarImage ?? "")
            cell.messageLabel.text = data.message
            cell.nameLabel.text = data.sender?.name
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = sections[indexPath.section]
        switch section {
        case .friend:
            return
        case .chat:
            let senders = array[indexPath.row].sender
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chat") as! ChatViewController
            vc.avatar = senders?.avatarImage ?? ""
            vc.name = senders?.name ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
}

extension MessageViewController: ChooseMesseageDelegate {
    func chooseChat(_ sender: People) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chat") as! ChatViewController
        vc.avatar = sender.avatarImage
        vc.name = sender.name
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension MessageViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredData = searchText.isEmpty ? arrayStringTittle : arrayStringTittle.filter({(dataString: String) -> Bool in
            return dataString.range(of: searchText, options: .caseInsensitive) != nil
        })
        filteredTour.removeAll()
        for name in filteredData {
            for mgs in array {
                if mgs.sender?.name == name {
                    filteredTour.append(mgs)
                    tableView.reloadData()
                }
            }
        }
        filteredData.removeAll()
        tableView.reloadData()
    }
}



