//
//  AccountViewController.swift
//  VietNamTravel
//
//  Created by admin on 12/14/21.
//

import UIKit
import Firebase

enum AccountFile {
    case profile
    case follow
    case follower
    case following
    case post
    case logout
    case infoUser
    init(index: Int) {
        switch index {
        case 0:
            self = .profile
        case 1:
            self = .follow
        case 2:
            self = .follower
        case 3:
            self = .following
        case 4:
            self = .post
        case 5:
            self = .logout
        case 6:
            self = .infoUser
        default:
            self = .follow
        }
    }
}

class AccountViewController: UIViewController {
    
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var loginButton: UIButton!{
        didSet {
            loginButton.layer.cornerRadius = loginButton.frame.height/2
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!{
        didSet {
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
            headerView.isUserInteractionEnabled = true
            headerView.addGestureRecognizer(tapGestureRecognizer)
        }
    }
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var avatarImage: UIImageView!{
        didSet {
            avatarImage.layer.cornerRadius = 20
        }
    }
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    var sections: [AccountFile] = []
    var arrayListPost = [Post]()
    private let maxHeaderHeight: CGFloat = 54.0
    private let minHeaderHeight: CGFloat = 0.0
    private var previousScrollOffset: CGFloat = 0.0
    var arrayFriend = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "PopupLoadingViewController") as? PopupLoadingViewController else { return }
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            vc.loadingActivity.stopAnimating()
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        headerHeightConstraint.constant = minHeaderHeight
        self.updateHeader()
        if Defaults.getInfoUser().email == "" {
            loginView.isHidden = false
        } else {
            
            getUserFirebase()
            loginView.isHidden = true
            nameLabel.text = Defaults.getInfoUser().name
            avatarImage.image = UIImage(named: Defaults.getInfoUser().avatarImage)
            setupUI()
//            tableView.reloadData()
        }
    }
    
    func setupUI() {
        arrayListPost = [Post(image: "hoiAn", content: "Dep", like: 1, comment: "ewe"),
                         Post(image: "hoiAn", content: "Dep", like: 1, comment: "ewe"),
                         Post(image: "hoiAn", content: "Dep", like: 1, comment: "ewe")]
//        arrayFriend = [People(avatar: "hoiAn", name: "Dinh Thi Diep"),
//                       People(avatar: "hoiAn", name: "Dinh Thu Chinh"),
//                       People(avatar: "hoiAn", name: "Bich Diep"),
//                       People(avatar: "hoiAn", name: "Hai Con"),
//                       People(avatar: "hoiAn", name: "Thang Phe"),
//                       People(avatar: "hoiAn", name: "Thuy"),
//                       People(avatar: "hoiAn", name: "Diep"),
//                       People(avatar: "hoiAn", name: "Thanh"),
//                       People(avatar: "hoiAn", name: "Duy Manh"),
//                       People(avatar: "hoiAn", name: "Duc Duy")]
//        arrayFriend = [User(fullName: "Dinh Thi Diep", avatarImage: "hoiAn", gender: "nu", birthday: "09/05/1998", address: "nam dinh", email: "diep@gmail.com"),
//                       User(fullName: "Dinh Thu Ching", avatarImage: "hoiAn", gender: "nu", birthday: "09/05/1998", address: "nam dinh", email: "diep@gmail.com")]
        
        self.navigationController?.navigationBar.isHidden = true
        sections.removeAll()
        sections.append(contentsOf: [.profile, .follower, .post, .logout])
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileTableViewCell")
        tableView.register(UINib(nibName: "FollowTableViewCell", bundle: nil), forCellReuseIdentifier: "FollowTableViewCell")
        tableView.register(UINib(nibName: "FollowerTableViewCell", bundle: nil), forCellReuseIdentifier: "FollowerTableViewCell")
        tableView.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "PostTableViewCell")
        tableView.register(UINib(nibName: "LogoutTableViewCell", bundle: nil), forCellReuseIdentifier: "LogoutTableViewCell")
        tableView.separatorStyle = .none
    }
    
    @objc func viewTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let viewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileUserViewController") as! ProfileUserViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func getUserFirebase() {
        arrayFriend.removeAll()
        let userDB = Database.database().reference().child("User")
        userDB.observe(.childAdded) {(snapShot) in
            let value = snapShot.value as! Dictionary<String, String>
            let fullname = value["Fullname"]!
            let avatarUser = value["AvatarImage"]!
            let gender = value["Gender"]!
            let birthday = value["Birthday"]!
            let address = value["Address"]!
            let email = value["Email"]!
            if fullname != Defaults.getInfoUser().name {
                self.arrayFriend.append(User(fullName: fullname, avatarImage: avatarUser, gender: gender, birthday: birthday, address: address, email: email))
            }
            self.tableView.reloadData()
        }
    }
    
    @IBAction func clickLoginButton(_ sender: Any) {
        let viewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: Methods

    private func canAnimateHeader(_ scrollView: UIScrollView) -> Bool {
        // Calculate height của scroll view khi header view bị collapse đến min height
        let scrollViewMaxHeight = scrollView.frame.height + headerHeightConstraint.constant - minHeaderHeight
        // Đảm bảo khi header bị collapse đến min height thì scroll view vẫn scroll được
        return scrollView.contentSize.height > scrollViewMaxHeight
    }

    private func setScrollPosition(_ position: CGFloat) {
        tableView.contentOffset = CGPoint(x: tableView.contentOffset.x, y: position)
    }

    private func scrollViewDidStopScrolling() {
        let range = maxHeaderHeight - minHeaderHeight
        let midPoint = minHeaderHeight + range / 2

        if headerHeightConstraint.constant > midPoint {
            // Expand header
            expandHeader()
        } else {
            // Collapse header
            collapseHeader()
        }
    }

    private func collapseHeader() {
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.headerHeightConstraint.constant = self.minHeaderHeight
            self.updateHeader()
            self.view.layoutIfNeeded()
        })
    }

    private func expandHeader() {
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.headerHeightConstraint.constant = self.maxHeaderHeight
            self.updateHeader()
            self.view.layoutIfNeeded()
        })
    }

    private func updateHeader() {
        // Tính khoảng cách giữa 2 value max và min height
        let range = maxHeaderHeight - minHeaderHeight
        // Tính khoảng offset hiện tại với min height
        let openAmount = headerHeightConstraint.constant - minHeaderHeight
        // Tính tỉ lệ phần trăm để animate, thay đổi UI element
        let percentage = openAmount / range

        // Animate UI theo tỉ lệ tính được
        headerView.alpha = percentage
//        print("===== ", percentage)
    }
    
}

extension AccountViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = sections[indexPath.section]
        switch section {
        case .profile:
            return 220
        case .follow:
            return 0
        case .follower:
            return 130
        case .following:
            return 0
        case .post:
            return 500
        case .logout:
            return 0
        case .infoUser:
            return 0
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollDiff = scrollView.contentOffset.y - previousScrollOffset

        // Điểm giới hạn trên cùng của scroll view
        let absoluteTop: CGFloat = 0.0
        // Điểm giới hạn dưới cùng của scroll view
        let absoluteBottom: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height

        let isScrollingDown = scrollDiff > 0 && scrollView.contentOffset.y > absoluteTop
        let isScrollingUp = scrollDiff < 0 && scrollView.contentOffset.y < absoluteBottom

        guard canAnimateHeader(scrollView) else {
            return
        }

        // Implement logic để animate header
        var newHeight = headerHeightConstraint.constant
        if isScrollingDown {
            newHeight = min(maxHeaderHeight, headerHeightConstraint.constant + abs(scrollDiff))
                
        } else if isScrollingUp {
            newHeight = max(minHeaderHeight, headerHeightConstraint.constant - abs(scrollDiff))
        }

        if newHeight != self.headerHeightConstraint.constant {
            headerHeightConstraint.constant = newHeight
            updateHeader()
            setScrollPosition(previousScrollOffset)
        }

        previousScrollOffset = scrollView.contentOffset.y
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // Kết thúc scroll
        scrollViewDidStopScrolling()
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            // Kết thúc scroll
            scrollViewDidStopScrolling()
        }
    }

}

extension AccountViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = sections[section]
        switch section {
        case .profile:
            return 1
        case .follow:
            return 0
        case .follower:
            return 1
        case .following:
            return 0
        case .post:
            return arrayListPost.count
        case .logout:
            return 0
        case .infoUser:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sections[indexPath.section]
        switch section {
        case .profile:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as? ProfileTableViewCell else {
                return UITableViewCell()
            }
            cell.delegate = self
            cell.fullnameLabel.text = Defaults.getInfoUser().name
            cell.avatarImage.image = UIImage(named: Defaults.getInfoUser().avatarImage)
            cell.nickNameLabel.text = Defaults.getInfoUser().email
            cell.phoneNumberLabel.text = Defaults.getInfoUser().phoneNumber
            cell.emailLabel.text = Defaults.getInfoUser().email
            return cell
        case .follow:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FollowTableViewCell", for: indexPath) as? FollowTableViewCell else {
                return UITableViewCell()
            }
            return cell
        case .follower:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FollowerTableViewCell", for: indexPath) as? FollowerTableViewCell else {
                return UITableViewCell()
            }
            cell.titleLabel.text = "Bạn bè"
            cell.arrayFriend = arrayFriend
            cell.collectionView.reloadData()
            cell.delegate = self
            return cell
        case .following:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FollowerTableViewCell", for: indexPath) as? FollowerTableViewCell else {
                return UITableViewCell()
            }
            cell.delegate = self
            cell.titleLabel.text = "Đang theo dõi"
            return cell
        case .post:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath) as? PostTableViewCell else {
                return UITableViewCell()
            }
//            let data = arrayListPost[indexPath.row]
            cell.nameLabel.text = Defaults.getInfoUser().name
            cell.avatarImage.image = UIImage(named: Defaults.getInfoUser().avatarImage)
            return cell
        
        case .logout:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "LogoutTableViewCell", for: indexPath) as? LogoutTableViewCell else {
                return UITableViewCell()
            }
            return cell
        case .infoUser:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let section = sections[indexPath.section]
            switch section {
            case .profile:
                return
            case .follow:
                return
            case .follower:
                return
            case .following:
                return
            case .post:
                return
            case .logout:
                tabBarController?.selectedIndex = 0
            case .infoUser:
                return
            }
    }
}

extension AccountViewController: FollowDelegate {
    
    func cellDelegate(_ people: User?) {
        let viewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        viewcontroller.people = people
        viewcontroller.arrayFriend = arrayFriend
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
}

extension AccountViewController: ProfileUserDelegate {
    func clickInfoUser() {
        let viewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileUserViewController") as! ProfileUserViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
}
