//
//  ProfileUserViewController.swift
//  VietNamTravel
//
//  Created by admin on 12/27/21.
//

import UIKit

class ProfileUserViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var arrayInfo = [InfoUser]()
    var sections: [AccountFile] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let data = Defaults.getInfoUser()
        arrayInfo = [InfoUser(icon: "location.north", value: data.address),
                     InfoUser(icon: "person.crop.circle", value: data.gender),
                     InfoUser(icon: "giftcard", value: data.birthday),
                     InfoUser(icon: "phone", value: data.phoneNumber),
                     InfoUser(icon: "envelope", value: data.email)]
    }
    
    func setupUI() {
//        for item in Defaults.userSessionKey {
//            arrayInfo.append(InfoUser(icon: "", value: Defaults.getInfoUser().))
//        }
        
        
        sections.removeAll()
        sections.append(contentsOf: [.profile, .infoUser, .logout])
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ProfileUserTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileUserTableViewCell")
        tableView.register(UINib(nibName: "InfoUserTableViewCell", bundle: nil), forCellReuseIdentifier: "InfoUserTableViewCell")
        tableView.register(UINib(nibName: "LogoutTableViewCell", bundle: nil), forCellReuseIdentifier: "LogoutTableViewCell")
        
        tableView.separatorStyle = .none
    }
    

}

extension ProfileUserViewController: UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let section = sections[indexPath.section]
        switch section {
        case .profile:
            return 300
        case .follow:
            return 0
        case .follower:
            return 0
        case .following:
            return 0
        case .post:
            return 0
        case .logout:
            return UITableView.automaticDimension
        case .infoUser:
            return 40
        }
    }
}

extension ProfileUserViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = sections[section]
        switch section {
        case .profile:
            return 1
        case .following:
            return 0
        case .post:
            return 0
        case .follow:
            return 0
        case .follower:
            return 0
        case .infoUser:
            return arrayInfo.count
        case .logout:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sections[indexPath.section]
        switch section {
        case .profile:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileUserTableViewCell", for: indexPath) as? ProfileUserTableViewCell else {
                return UITableViewCell()
            }
            let data = Defaults.getInfoUser()
            cell.nameLabel.text = data.name
            cell.avatarImage.image = UIImage(named: data.avatarImage)
            cell.bgImage.image = UIImage(named: data.avatarImage)
            cell.nickNameLabel.text = data.email
            cell.messageButton.isHidden = true
            cell.followButton.isHidden = true
            return cell
        case .following:
            return UITableViewCell()
        case .post:
            return UITableViewCell()
        case .follow:
            return UITableViewCell()
        case .follower:
            return UITableViewCell()
        case .infoUser:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "InfoUserTableViewCell", for: indexPath) as? InfoUserTableViewCell else {
                return UITableViewCell()
            }
            let data = arrayInfo[indexPath.row]
            cell.iconImage.image = UIImage(systemName: data.icon)
            cell.titleLabel.text = data.value
            return cell
        case .logout:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "LogoutTableViewCell", for: indexPath) as? LogoutTableViewCell else {
                return UITableViewCell()
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = sections[indexPath.section]
        switch section {
        case .profile:
            return
        case .following:
            return
        case .post:
            return
        case .follow:
            return
        case .follower:
            return
        case .infoUser:
            return
        case .logout:
            Defaults.clearUserData()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
