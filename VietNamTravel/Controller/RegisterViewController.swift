//
//  RegisterViewController.swift
//  VietNamTravel
//
//  Created by admin on 12/9/21.
//

import UIKit
import Toast_Swift
import Firebase

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        backgroundView.backgroundColor = UIColor(white: 0.3, alpha: 0.4)
        registerButton.layer.cornerRadius = 17.5
        confirmPasswordTextField.delegate = self
    }
    
    @IBAction func clickRegisterButton(_ sender: Any) {
        if emailTextField.text == "" {
            self.view.makeToast("Enter your email")
            return
        }
        if passwordTextField.text == "" {
            self.view.makeToast("Enter your password")
            return
        }
        if confirmPasswordTextField.text == "" {
            self.view.makeToast("Enter your confirm pasword")
            return
        }
        
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            if(error == nil){
                self.view.makeToast("Register Success")
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
                vc.email = self.emailTextField.text
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
//                debugPrint(error)
                self.view.makeToast("Error")
            }
        }
        
    }
    
    @IBAction func clickBackButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension RegisterViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if passwordTextField.text != confirmPasswordTextField.text {
            self.view.makeToast("Confirm password error")
            return
        }
    }
}
