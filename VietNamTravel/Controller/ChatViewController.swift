//
//  ChatViewController.swift
//  VietNamTravel
//
//  Created by admin on 12/24/21.
//

import UIKit
import Firebase
import FirebaseDatabase

class ChatViewController: UIViewController {
    
    @IBOutlet weak var avatarSenderImage: UIImageView!{
        didSet {
            avatarSenderImage.layer.cornerRadius = avatarSenderImage.frame.height/2
        }
    }
    @IBOutlet weak var nameSenderLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inboxTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    var name = ""
    var nameUser = Defaults.getInfoUser().name
    var avatar = ""
    var arrayMessage = [Message]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        avatarSenderImage.image = UIImage(named: avatar)
        nameSenderLabel.text = name
        setupUI()
    }
    
    func setupUI() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        tableView.delegate = self
        tableView.dataSource = self
        inboxTextField.delegate = self
        sendButton.isEnabled = false
        tableView.register(UINib(nibName: "SenderTableViewCell", bundle: nil), forCellReuseIdentifier: "SenderTableViewCell")
        tableView.register(UINib(nibName: "ChatTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatTableViewCell")
        getMessage()
        tableView.separatorStyle = .none
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func clickBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickSendButton(_ sender: Any) {
        if self.inboxTextField.text != "" {
            let messageDB = Database.database().reference().child("Messages")
            let messageDict = ["Sender" : name, "User" : nameUser, "MessageBody" : inboxTextField.text!]
            messageDB.childByAutoId().setValue(messageDict) {(error, ref) in
                if error != nil {
                    debugPrint(error)
                } else {
                    
                    
                    debugPrint("Msg saved successfully")
                    self.inboxTextField.text = ""
                    self.inboxTextField.isEnabled = false
                    self.sendButton.isEnabled = false
                    self.inboxTextField.isEnabled = true
                    
                }
            }
            
            if !arrayMessage.isEmpty {
                scrollToBottom()
            }
        }
    }
    
    func scrollToBottom(){
        let indexPath = IndexPath(row: self.arrayMessage.count-1, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        self.tableView.layoutIfNeeded()
    }
    
    func getMessage() {
        let messageDB = Database.database().reference().child("Messages")
        messageDB.observe(.childAdded) {(snapShot) in
            let value = snapShot.value as! Dictionary<String, String>
            let text = value["MessageBody"]!
            let sender = value["Sender"]!
            let user = value["User"]!
            var msg = Message(user: People(avatar: "", name: ""), sender: People(avatar: "", name: ""), message: "")
            msg.user?.name = user
            msg.sender?.name = sender
            msg.message = text
            if (msg.user?.name == self.nameUser && msg.sender?.name == self.name) || (msg.user?.name == self.name && msg.sender?.name == self.nameUser) {
                self.arrayMessage.append(msg)
                self.tableView.reloadData()
            }
            
            
            
        }
    }
    
}

extension ChatViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ChatViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMessage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if nameUser == arrayMessage[indexPath.row].user?.name {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatTableViewCell", for: indexPath) as! ChatTableViewCell
            let data = arrayMessage[indexPath.row]
            cell.messageBodyLabel.text = data.message
            cell.avatarImage.image = UIImage(named: Defaults.getInfoUser().avatarImage)
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SenderTableViewCell", for: indexPath) as! SenderTableViewCell
            let data = arrayMessage[indexPath.row]
            cell.messageBodyLabel.text = data.message
            cell.avatarImage.image = UIImage(named: avatar)
            return cell
        }    }
    
}

extension ChatViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        inboxTextField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if inboxTextField.text == "" {
            sendButton.isEnabled = true
        } else {
            sendButton.isEnabled = false
        }
    }
}
