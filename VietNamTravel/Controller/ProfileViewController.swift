//
//  ProfileViewController.swift
//  VietNamTravel
//
//  Created by admin on 12/15/21.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.layer.cornerRadius = 8
            tableView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
    }
    
    var avatar = "hoiAn"
    var arrayInfo = [InfoUser]()
    var sections: [AccountFile] = []
    var arrayListPost = [Post]()
    var height: CGFloat = 500
    var isClickCommentBuuton = false
    var people: User?
    var arrayFriend = [User]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        arrayListPost = [Post(image: "hoiAn", content: "Dep", like: 1, comment: "ewe"),
                         Post(image: "hoiAn", content: "Dep", like: 1, comment: "ewe"),
                         Post(image: "hoiAn", content: "Dep", like: 1, comment: "ewe")]
        arrayInfo = [InfoUser(icon: "phone", value: "03563257777"),
                     InfoUser(icon: "envelope", value: "Dieptit@gmail.com")]
        sections.removeAll()
        sections.append(contentsOf: [.profile,.infoUser, .following, .post])
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ProfileUserTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileUserTableViewCell")
        tableView.register(UINib(nibName: "FollowerTableViewCell", bundle: nil), forCellReuseIdentifier: "FollowerTableViewCell")
        tableView.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "PostTableViewCell")
        tableView.register(UINib(nibName: "InfoUserTableViewCell", bundle: nil), forCellReuseIdentifier: "InfoUserTableViewCell")
        tableView.separatorStyle = .none
    }
    
}

extension ProfileViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let section = sections[indexPath.section]
        switch section {
        case .profile:
            return 300
        case .follow:
            return 0
        case .follower:
            return 0
        case .following:
            return 150
        case .post:
            return height
        case .logout:
            return 0
        case .infoUser:
            return 40
        }
    }
}

extension ProfileViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = sections[section]
        switch section {
        case .profile:
            return 1
        case .following:
            return 1
        case .post:
            return arrayListPost.count
        case .follow:
            return 0
        case .follower:
            return 0
        case .logout:
            return 0
        case .infoUser:
            return arrayInfo.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sections[indexPath.section]
        switch section {
        case .profile:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileUserTableViewCell", for: indexPath) as? ProfileUserTableViewCell else {
                return UITableViewCell()
            }
            cell.avatarImage.image = UIImage(named: people?.avatarImage ?? "")
            cell.nameLabel.text = people?.fullName
            cell.delegate = self
            return cell
        case .following:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FollowerTableViewCell", for: indexPath) as? FollowerTableViewCell else {
                return UITableViewCell()
            }
            cell.arrayFriend = arrayFriend
            cell.delegate = self
            cell.titleLabel.text = "Bạn bè"
            return cell
        case .post:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath) as? PostTableViewCell else {
                return UITableViewCell()
            }
            cell.delegate = self
            return cell
        case .logout:
            return UITableViewCell()
        case .follow:
            return UITableViewCell()
        case .follower:
            return UITableViewCell()
        case .infoUser:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "InfoUserTableViewCell", for: indexPath) as? InfoUserTableViewCell else {
                return UITableViewCell()
            }
            let data = arrayInfo[indexPath.row]
            cell.iconImage.image = UIImage(systemName: data.icon)
            cell.titleLabel.text = data.value
            return cell
        }
    }
}

extension ProfileViewController: FollowDelegate {
    func cellDelegate(_ people: User?) {
        let viewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
}

extension ProfileViewController: PostCellDelegate {
    func isClickButtonCommnent(_ isClick: Bool, _ statusHeight: CGFloat, _ commentHeeight: CGFloat) {
        if isClick {
            if commentHeeight < 300 {
                // show all comment
                height = 800
            } else {
                height = 700
            }
            tableView.reloadData()
        } else {
            height = 500
            tableView.reloadData()
        }
    }
    
}

extension ProfileViewController: ProfileUserTableViewCellDelegate {
    func clickChatButton() {
        tabBarController?.selectedIndex = 2
    }
    
    
}
