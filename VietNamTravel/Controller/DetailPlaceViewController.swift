//
//  DetailPlaceViewController.swift
//  VietNamTravel
//
//  Created by admin on 12/7/21.
//

import UIKit

enum AreaTable: Int {
    case info
    case maps
    case suggestions
    
    init(index: Int) {
        switch index {
        case 0:
            self = .info
        case 1:
            self = .maps
        case 2:
            self = .suggestions
        default:
            self = .suggestions
        }
    }
}

class DetailPlaceViewController: UIViewController{
    
    @IBOutlet weak var hearderView: UIView!
    @IBOutlet weak var areaTableView: UITableView!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var backButton: UIButton!
    
    var imageArr = ["BackgroundLogin", "background", "background2"]
    var sections: [AreaTable] = []
    var nameTitle = "Hồ Hoàn Kiếm"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        areaTableView.layer.cornerRadius = 8
        areaTableView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        configurePageControl()
        sections.removeAll()
        sections.append(contentsOf: [.info , .maps, .suggestions])
        areaTableView.delegate = self
        areaTableView.dataSource = self
        areaTableView.register(UINib(nibName: "InfoTableViewCell", bundle: nil), forCellReuseIdentifier: "InfoTableViewCell")
        areaTableView.register(UINib(nibName: "MapsTableViewCell", bundle: nil), forCellReuseIdentifier: "MapsTableViewCell")
        areaTableView.register(UINib(nibName: "SuggestionsTableViewCell", bundle: nil), forCellReuseIdentifier: "SuggestionsTableViewCell")
        areaTableView.separatorStyle = .none
    }
    
    func configurePageControl() {
        self.pageControl.numberOfPages = imageArr.count
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.red
//        self.pageControl.pageIndicatorTintColor = UIColor.black
//        self.pageControl.currentPageIndicatorTintColor = UIColor.white
        
    }
    
    @IBAction func clickPageControl(_ sender: Any) {
            self.headerImage.image = UIImage(named: imageArr[pageControl.currentPage])

    }
    
    @IBAction func clickBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension DetailPlaceViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = sections[section]
        switch section {
        case .info:
            return 1
        case .maps:
            return 1
        case .suggestions:
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sections[indexPath.section]
        switch section {
        case .info:
            guard let cell = areaTableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as? InfoTableViewCell else {
                return UITableViewCell()
            }
            return cell
        case .maps:
            guard let cell = areaTableView.dequeueReusableCell(withIdentifier: "MapsTableViewCell", for: indexPath) as? MapsTableViewCell else {
                return UITableViewCell()
            }
            cell.nameTitle = nameTitle
            return cell
        case .suggestions:
            guard let cell = areaTableView.dequeueReusableCell(withIdentifier: "SuggestionsTableViewCell", for: indexPath) as? SuggestionsTableViewCell else {
                return UITableViewCell()
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = sections[indexPath.section]
        switch section {
        case .info:
            return
        case .maps:
            guard let vc = storyboard?.instantiateViewController(withIdentifier: "PopupMapViewController") as? PopupMapViewController else { return }
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
            vc.nameTitle = nameTitle
                present(vc, animated: true)
        case .suggestions:
            return
        }
    }
    
}

extension DetailPlaceViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = sections[indexPath.section]
        switch section {
        case .info:
            return UITableView.automaticDimension
        case .maps:
            return 70
        case .suggestions:
            return UITableView.automaticDimension
        }
    }
}
